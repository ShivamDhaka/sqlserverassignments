-- Assignment 8

create database WorkerRecords
use WorkerRecords

create table Worker (WORKER_ID int primary key, 
FIRST_NAME varchar(25) not null,
LAST_NAME varchar(25),
SALARY int check(SALARY between 10000 and 25000),
JOINING_DATE datetime,
DEPARTMENT varchar(25) check(DEPARTMENT in ('HR', 'Sales', 'Accts', 'IT')
))

insert into Worker (WORKER_ID, FIRST_NAME, LAST_NAME, SALARY, JOINING_DATE, DEPARTMENT)
values
(001, 'Monika', 'Arora', 15000, '2023-05-03', 'HR'),
(002, 'Ram', 'Singh', 15000, '2023-05-03', 'Sales'),
(003, 'Shyam', 'Singh', 15000, '2023-05-03', 'Accts'),
(004, 'Babu', 'Bhaiya', 15000, '2023-05-03', 'IT'),
(005, 'Totla', 'Tiwari', 15000, '2023-05-03', 'Sales')



--Q.1
select FIRST_NAME as WORKER_NAME from Worker

--Q.2
select UPPER(FIRST_NAME) from Worker

-- Q.3
select distinct DEPARTMENT from Worker

--Q.4
select SUBSTRING(FIRST_NAME, 1, 3) from Worker 

--Q.5
select charindex('a', FIRST_NAME) from Worker

--Q.6
select RTRIM(DEPARTMENT) from Worker

--Q.7
select LTRIM(DEPARTMENT) from worker

--Q.8
select distinct len(DEPARTMENT) from Worker

--Q.9
select Replace(FIRST_NAME, 'a', 'A') from Worker

--Q.10
select CONCAT(FIRST_NAME,' ', LAST_NAME) as COMPLETE_NAME from Worker

--Q.11
select * from Worker order by FIRST_NAME

--Q.12
select * from Worker order by FIRST_NAME asc,DEPARTMENT desc

--Q.13
select * from Worker where FIRST_NAME in ('Babu', 'Totla')

--Q.14
select * from Worker where FIRST_NAME not in ('Babu','Totla')

--Q.15
select * from Worker where DEPARTMENT in ('IT')

--Q.16
select * from Worker where FIRST_NAME like '%a%'

--Q.17
select * from Worker where FIRST_NAME like '%a'

--Q.18
select * from Worker where FIRST_NAME like '%h' and len(first_name)=6

--Q.19
select * from Worker where SALARY between 10000 and 18000

--Q.20
select * from Worker where year(JOINING_DATE) = 2014 and month(JOINING_DATE) = 2

--Q.21
select COUNT(*) from Worker where DEPARTMENT='Sales'
